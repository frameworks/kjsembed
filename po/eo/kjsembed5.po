# translation of kjsembed5.pot to Esperanto
# Copyright (C) 1998,2002, 2003, 2004, 2005, 2007, 2008 Free Software Foundation, Inc.
# Wolfram Diestel <wolfram@steloj.de>, 1998.
# Heiko Evermann <heiko@evermann.de>, 2002, 2003.
# Matthias Peick <matthias@peick.de>, 2004, 2005.
# Oliver Kellogg <okellogg@users.sourceforge.net>,2007.
# Cindy McKee <cfmckee@gmail.com>, 2007, 2008.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
# Michael Moroni <michael.moroni@mailoo.org>, 2012.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: kdelibs4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-10-24 02:12+0200\n"
"PO-Revision-Date: 2023-01-05 10:00+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-doc@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kjsembed/builtins.cpp:83 kjsembed/builtins.cpp:117
#, kde-format
msgid "Error encountered while processing include '%1' line %2: %3"
msgstr "Vidis eraron dum procezado de anigu '%1' linio %2: %3"

#: kjsembed/builtins.cpp:87
#, kde-format
msgid "include only takes 1 argument, not %1."
msgstr "anigu nur prenas 1 argumenton, ne %1."

#: kjsembed/builtins.cpp:120
#, kde-format
msgid "File %1 not found."
msgstr "Dosiero %1 ne troviĝis."

#: kjsembed/builtins.cpp:125
#, kde-format
msgid "library only takes 1 argument, not %1."
msgstr "biblioteko nur prenas 1 argumenton, ne %1."

#: kjsembed/builtins.cpp:137
#, kde-format
msgid "Alert"
msgstr "Averto"

#: kjsembed/builtins.cpp:147
#, kde-format
msgid "Confirm"
msgstr "Konfirmi"

#: kjsembed/eventproxy.cpp:110
#, kde-format
msgid "Bad event handler: Object %1 Identifier %2 Method %3 Type: %4."
msgstr "Malbona eventotraktilo: Objekto %1 Identigilo %2 Metodo %3 Tipo: %4."

#: kjsembed/eventproxy.cpp:133
#, kde-format
msgid "Exception calling '%1' function from %2:%3:%4"
msgstr "Escepto dum voko de '%1' funkcio el %2:%3:%4"

#: kjsembed/fileio.cpp:69
#, kde-format
msgid "Could not open file '%1'"
msgstr "Ne eblis malfermi dosieron '%1'"

#: kjsembed/fileio.cpp:106
#, kde-format
msgid "Could not create temporary file."
msgstr "Ne eblis krei labordosieron."

#: kjsembed/kjsembed.cpp:275 kjsembed/kjsembed.cpp:298
#, kde-format
msgid "%1 is not a function and cannot be called."
msgstr "%1 ne estas funkcio, do ne povas esti vokita."

#: kjsembed/object_binding.h:214 kjsembed/qobject_binding.cpp:788
#, kde-format
msgid "%1 is not an Object type"
msgstr "%1 ne estas objektotipo"

#: kjsembed/qaction_binding.cpp:56
#, kde-format
msgid "Action takes 2 args."
msgstr "Ago prenas 2 argumentojn."

#: kjsembed/qaction_binding.cpp:78
#, kde-format
msgid "ActionGroup takes 2 args."
msgstr "AgoGrupo prenas 2 argumentojn."

#: kjsembed/qaction_binding.cpp:83
#, kde-format
msgid "Must supply a valid parent."
msgstr "Devas doni validan patron."

#: kjsembed/qformbuilder_binding.cpp:56
#, kde-format
msgid "There was an error reading the file '%1'"
msgstr "Eraro okazis dum legado de la dosiero '%1'"

#: kjsembed/qformbuilder_binding.cpp:64
#, kde-format
msgid "Could not read file '%1'"
msgstr "Ne eblis legi dosieron '%1'"

#: kjsembed/qformbuilder_binding.cpp:67 kjsembed/quiloader_binding.cpp:123
#, kde-format
msgid "Must supply a filename."
msgstr "Devas doni dosiernomon."

#: kjsembed/qlayout_binding.cpp:86
#, kde-format
msgid "'%1' is not a valid QLayout."
msgstr "'%1' ne estas valida QLayout."

#: kjsembed/qlayout_binding.cpp:92
#, kde-format
msgid "Must supply a layout name."
msgstr "Devas doni aranĝan nomon."

#: kjsembed/qobject_binding.cpp:140
#, kde-format
msgid "Wrong object type."
msgstr "Malĝusta objekta tipo."

#: kjsembed/qobject_binding.cpp:147
#, kde-format
msgid "First argument must be a QObject."
msgstr "Unua argumento devas esti QObjekto."

#: kjsembed/qobject_binding.cpp:181
#, kde-format
msgid "Incorrect number of arguments."
msgstr "Malĝusta nombro de argumentoj."

#: kjsembed/qobject_binding.cpp:409
#, kde-format
msgid "The slot asked for %1 argument"
msgid_plural "The slot asked for %1 arguments"
msgstr[0] ""
msgstr[1] ""

#: kjsembed/qobject_binding.cpp:410
#, kde-format
msgid "but there is only %1 available"
msgid_plural "but there are only %1 available"
msgstr[0] "sed estas nur %1 disponebla"
msgstr[1] "sed estas nur %1 disponeblaj"

#: kjsembed/qobject_binding.cpp:411
#, kde-format
msgctxt ""
"%1 is 'the slot asked for foo arguments', %2 is 'but there are only bar "
"available'"
msgid "%1, %2."
msgstr "%1, %2."

#: kjsembed/qobject_binding.cpp:628
#, kde-format
msgid "Failure to cast to %1 value from Type %2 (%3)"
msgstr "Malsukcesis sendi al %1 valoron de tipo %2 (%3)"

#: kjsembed/qobject_binding.cpp:660
#, kde-format
msgid "No such method '%1'."
msgstr "Neniu metodo '%1'."

#: kjsembed/qobject_binding.cpp:689
#, kde-format
msgid "Call to method '%1' failed, unable to get argument %2: %3"
msgstr "Voko al metodo '%1' malsukcesis, ne povas ricevi argumenton %2: %3"

#: kjsembed/qobject_binding.cpp:726
#, kde-format
msgid "Call to '%1' failed."
msgstr "Voko al '%1' fiaskis."

#: kjsembed/qobject_binding.cpp:792
#, kde-format
msgid "Could not construct value"
msgstr "Ne eblis konstrui valoron"

#: kjsembed/quiloader_binding.cpp:48
#, kde-format
msgid "Not enough arguments."
msgstr "Ne sufiĉe de argumentoj."

#: kjsembed/quiloader_binding.cpp:67
#, kde-format
msgid "Failed to create Action."
msgstr "Ne eblis krei agon."

#: kjsembed/quiloader_binding.cpp:81
#, kde-format
msgid "Failed to create ActionGroup."
msgstr "Ne eblis krei AgoGrupon."

#: kjsembed/quiloader_binding.cpp:89
#, kde-format
msgid "No classname specified"
msgstr "Ne difinis klasonomon"

#: kjsembed/quiloader_binding.cpp:97
#, kde-format
msgid "Failed to create Layout."
msgstr "Ne eblis krei aranĝon."

#: kjsembed/quiloader_binding.cpp:106
#, kde-format
msgid "No classname specified."
msgstr "Ne difinis klasonomon."

#: kjsembed/quiloader_binding.cpp:114
#, kde-format
msgid "Failed to create Widget."
msgstr "Ne eblis krei fenestraĵon."

#: kjsembed/quiloader_binding.cpp:128
#, kde-format
msgid "Could not open file '%1': %2"
msgstr "Ne eblis malfermi dosieron '%1': %2"

#: kjsembed/quiloader_binding.cpp:136
#, kde-format
msgid "Failed to load file '%1'"
msgstr "Ne eblis ŝargi dosieron '%1'"

#: kjsembed/qwidget_binding.cpp:161
#, kde-format
msgid "'%1' is not a valid QWidget."
msgstr "'%1' ne estas valida QWidget."

#: kjsembed/qwidget_binding.cpp:170
#, kde-format
msgid "Must supply a widget name."
msgstr "Devas doni nomon por la fenestraĵo."

#: kjsembed/slotproxy.cpp:117
#, kde-format
msgid "Bad slot handler: Object %1 Identifier %2 Method %3 Signature: %4."
msgstr ""
"Malbona fakotraktilo: Objekto %1 Identigilo %2 Metodo %3 Subskribo: %4."

#: kjsembed/slotproxy.cpp:140
#, kde-format
msgid "Exception calling '%1' slot from %2:%3:%4"
msgstr "Escepto dum voko de '%1' fako el %2:%3:%4"
